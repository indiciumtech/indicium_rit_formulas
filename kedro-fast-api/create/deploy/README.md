# Description

Formula description

## Command

```bash
rit kedro-fast-api create deploy
```

## Requirements
Cookiecutter >= 2.0.0

## Demonstration
This formula just use one config file as input, the config file must look similar to this:

```
{
    "cookiecutter":
        {
            "deploy": "deploy",
            "aws_account_id": "77711777751811",
            "aws_profile": "brognoli",
            "aws_region": "us-east-1",
            "aws_bucket": "indicium-brognoli-data",
            "listener_rule": "fast-api.*",
            "listener_rule_dev": "fast-api-dev.*",
            "vpc_id": "vpc-0ff1dd3092c1151ac56",
            "alb_listener_arn": "arn:aws:elasticloadbalancing:us-east-1:22777117777518:listener/app/indicium-main-alb/440bfef7e2e85421/b8695aa146c1c4ad",
            "execution_role_arn": "arn:aws:iam::77711777722518:role/indicium-airflow-prod-ecs-task-role",
            "ecs_cluster_id": "indicium-terraform-cluster",
            "ecr_repository_name": "kedro-fast-api",
            "task_definition_image": "77711777751228.dkr.ecr.us-east-1.amazonaws.com/kedro-fast-api",
            "task_definition_image_dev": "77711777722518.dkr.ecr.us-east-1.amazonaws.com/kedro-fast-api_dev"
        }
}
```