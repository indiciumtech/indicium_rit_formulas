#!/bin/sh

runFormula() {
  echo "Make sure to have cookiecutter installed"
  cookiecutter git@bitbucket.org:indiciumtech/kedro_fast_api_deploy_app_template.git --replay-file "$RIT_VARIABLES_PATH"
  mv deploy/bitbucket-pipelines.yml .
}
