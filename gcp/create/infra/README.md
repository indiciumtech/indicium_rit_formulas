# Description

Formula used to create a new infra repository, utilizing Cookiecutter. To run this formula, create a JSON file, containing the values that will be used to fill cookiecutter variables, such as shown below.

```json
{
    "cookiecutter":
        {
            "project_name": "",
            "client_name": "",
            "project_id": "",
            "module_version": "",
            "gcp_region": "",
            "sql_db_instance": "",
            "gke_node_instance": "",
            "vpn_instance": "",
            "allow_bitbucket_to_db": ""
        }
}
```

For more informations about the values of each variable, check the referenced [cookiecutter infra repository](https://bitbucket.org/indiciumtech/cookiecutter_infra_gcp/src/master/).

## Command

```bash
rit aws create infra
```

## Requirements

Cookiecutter >= 2.0.0

## Demonstration
