# Description

Formula used to create a new infra repository, utilizing Cookiecutter. To run this formula, create a JSON file, containing the values that will be used to fill cookiecutter variables, such as shown below.

```json
{
    "cookiecutter":
        {
            "project_name": "",
            "client_name": "",
            "aws_region": "",
            "module_version": "",
            "ecs_instance_type": "",
            "rds_instance_type": "",
            "ecs_ami": "",
            "pritunl_ami": ""
        }
}
```

For more informations about the values of each variable, check the referenced [cookiecutter infra repository](https://bitbucket.org/indiciumtech/cookiecutter_infra/src/master/).

## Command

```bash
rit aws create infra
```

## Requirements

Cookiecutter >= 2.0.0

## Demonstration
