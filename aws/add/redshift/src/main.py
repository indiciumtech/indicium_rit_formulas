#!/usr/bin/python3
import os

from formula import formula


redshift_conf = {
    'module_version': os.environ.get('RIT_MODULE_VERSION'),
    'client_name': os.environ.get('RIT_CLIENT_NAME'),
    'cluster_type': os.environ.get('RIT_REDSHIFT_CLUSTER_TYPE'),
    'node_type': os.environ.get('RIT_REDSHIFT_NODE_TYPE'),
    'skip_final_snapshot': os.environ.get('RIT_SKIP_FINAL_SNAPSHOT'),
    'allow_bitbucket_to_redshift': os.environ.get('RIT_ALLOW_BITBUCKET_TO_REDSHIFT'),
    'disk_usage_threshold': os.environ.get('RIT_DISK_USAGE_THRESHOLD'),
    'cpu_threshold': os.environ.get('RIT_CPU_USAGE_THRESHOLD'),
    'high_usage_disk_statistic': os.environ.get('RIT_DISK_USAGE_STATISTIC'),
    'cpu_alarm_statistic': os.environ.get('RIT_CPU_USAGE_STATISTIC')
}

infra_path = os.environ.get('RIT_INFRA_PATH')

add_sns = os.environ.get('RIT_ADD_SNS')

sns_conf = {
    'aws_region': os.environ.get('RIT_AWS_REGION', ''),
    'module_version': os.environ.get('RIT_MODULE_VERSION'),
    'client_name': os.environ.get('RIT_CLIENT_NAME', ''),
    'monthly_billing_threshold': os.environ.get('RIT_MONTHLY_BILLING_THRESHOLD', ''),
    'currency': os.environ.get('RIT_BILLING_CURRENCY', ''),
    'aws_account_id': os.environ.get('RIT_AWS_ACCOUNT_ID', '')
}

formula.Run(redshift_conf, add_sns, sns_conf, infra_path)
