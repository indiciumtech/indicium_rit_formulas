#!/usr/bin/python3
from distutils.util import strtobool
import os
from pathlib import Path

from jinja2 import Template


def get_template_path(filename: str):
    return Path(__file__).parents[1]/f'templates/{filename}'


def Run(redshift_conf: dict, add_sns: str, sns_conf: dict, infra_path: str):

    paths = {}
    if strtobool(add_sns):
        paths.update({
            get_template_path('cloudwatch_alarm_module.j2'): f'{infra_path}/infra/infra.tf'
        })

    paths.update({
        get_template_path('redshift_tf.j2'): f'{infra_path}/infra/infra.tf',
        get_template_path('redshift_sg_tf.j2'): f'{infra_path}/infra/security_groups.tf',
        get_template_path('redshift_vars_tf.j2'): f'{infra_path}/infra/vars.tf',
        get_template_path('redshift_example_env.j2'): f'{infra_path}/.example.env'
    })


    for key, value in paths.items():
        path = key
        template = Template(path.open().read())
        rendered = template.render(
            {
                'sns': sns_conf,
                'redshift': redshift_conf
            }
        )

        try:
            with open(value, 'a') as file:
                file.write(rendered)
        except FileNotFoundError:
            print("You must be in the root directory of the infra.")
