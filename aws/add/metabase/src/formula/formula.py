#!/usr/bin/python3
import os
from pathlib import Path

import hcl
from jinja2 import Template


def get_template_path(filename: str):
    return Path(__file__).parents[1]/f'templates/{filename}'


def check_if_env_exists(metabase_env: str, infra_path: str):
    """Check if the specified metabase env already exists

    :param metabase_env: Metabase environment sent when filling Ritchie
        variables
    :type metabase_env: str
    :param infra_path: Path to the local host infra
    :type infra_path: str
    :raises ValueError: Raises error when the metabase environment provided
        already exists
    """
    try:
        with open(f'{infra_path}/infra/metabase.tf', 'r') as f:
            obj = hcl.load(f)
            modules = []
            for key, value in obj.get('module').items():
                modules.append(key)
    except FileNotFoundError:
        pass

    if f'metabase_{metabase_env}' in modules:
        raise ValueError(f'The metabase environment is already in use.')
    else:
        pass


def check_port_in_use(metabase_port: str, infra_path: str):
    """Check if the specified metabase port is already in use

    :param metabase_port: Metabase port sent when filling Ritchie
        variables
    :type metabase_port: str
    :param infra_path: Path to the local host infra
    :type infra_path: str
    :raises ValueError: Raises error when the metabase port provided is already
        in use
    """
    try:
        with open(f'{infra_path}/infra/metabase.tf', 'r') as f:
            obj = hcl.load(f)
            used_ports = []
            for key, value in obj.get('module').items():
                used_ports.append(value.get('metabase_port'))
    except FileNotFoundError:
        pass

    if str(metabase_port) in used_ports:
        raise ValueError(f'The host port you assigned is already in use.')
    else:
        pass


def check_first_metabase(metabase_vars: dict, metabase_tf_path: str) -> dict:
    """Check if it is the first metabase created

    :param metabase_vars: Dictionary containing metabase jinja variables.
    :type metabase_vars: dict
    :param metabase_tf_path: Path in which the metabase.tf is or will bi
        located on the user system.
    :type metabase_tf_path: str
    :return: Dictionary with updated metabase jinja variables.
    :rtype: dict
    """

    if not os.path.exists(metabase_tf_path):
        metabase_vars.update(
            {
                'first_metabase': True
            }
        )

    return metabase_vars


def Run(metabase_vars: dict, infra_path: str):

    metabase_tf_path = f'{infra_path}/infra/metabase.tf'

    paths = {
        get_template_path('metabase_module_tf.j2'): metabase_tf_path,
        get_template_path('metabase_vars_tf.j2'): f'{infra_path}/infra/vars.tf',
        get_template_path('project_conf.j2'): f'{infra_path}/project_conf.sh'
    }

    check_if_env_exists(
        metabase_env=metabase_vars.get('metabase_env'),
        infra_path=infra_path
    )
    check_port_in_use(
        metabase_port=metabase_vars.get('metabase_host_port'),
        infra_path=infra_path
    )
    metabase_conf = check_first_metabase(metabase_vars, metabase_tf_path)

    for key, value in paths.items():
        path = key
        template = Template(path.open().read())
        rendered = template.render(mb=metabase_conf)

        try:
            with open(value, 'a') as file:
                file.write(rendered)
        except FileNotFoundError:
            print("You must be in the root directory of the infra.")
