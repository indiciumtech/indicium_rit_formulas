#!/usr/bin/python3
import os

from formula import formula

metabase_conf = {
    'client_name': os.environ.get('RIT_CLIENT_NAME'),
    'metabase_module_version': os.environ.get('RIT_METABASE_MODULE_VERSION'),
    'metabase_image_version': os.environ.get('RIT_METABASE_VERSION'),
    'metabase_env': os.environ.get('RIT_METABASE_ENV'),
    'metabase_host_port': os.environ.get('RIT_METABASE_PORT'),
    'metabase_db_host': os.environ.get('RIT_METABASE_DB_HOST')
}

infra_path = os.environ.get('RIT_INFRA_PATH')

formula.Run(metabase_conf, infra_path)
