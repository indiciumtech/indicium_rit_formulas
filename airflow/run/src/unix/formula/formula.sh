#!/bin/sh

runFormula() {
  if [[ -z "${AIRFLOW_HOME}" ]]; then
    echo "environment variable AIRFLOW_HOME is not set"
    exit 1
  fi
  echo "AIRFLOW_HOME set to $AIRFLOW_HOME"

  if [ ! -d "$AIRFLOW_HOME" ]; then
    echo "AIRFLOW_HOME: $AIRFLOW_HOME does not exists"
    exit 1
  fi

  cd unix/formula
  docker build -t airflow-2 -f Dockerfile_airflow .
  docker-compose up
}

echoColor() {
  case $1 in
    red)
      echo "$(printf '\033[31m')$2$(printf '\033[0m')"
      ;;
    green)
      echo "$(printf '\033[32m')$2$(printf '\033[0m')"
      ;;
    yellow)
      echo "$(printf '\033[33m')$2$(printf '\033[0m')"
      ;;
    blue)
      echo "$(printf '\033[34m')$2$(printf '\033[0m')"
      ;;
    cyan)
      echo "$(printf '\033[36m')$2$(printf '\033[0m')"
      ;;
    esac
}
